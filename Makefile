.PHONY: clean train-nlu train-core cmdline server

TEST_PATH=./

help:
	@echo "    clean"
	@echo "        Remove python artifacts and build artifacts."
	@echo "    train-nlu"
	@echo "        Trains a new nlu model using the projects Rasa NLU config"
	@echo "    train-core"
	@echo "        Trains a new dialogue model using the story training data"
	@echo "    action-server"
	@echo "        Starts the server for custom action."
	@echo "    cmdline"
	@echo "       This will load the assistant in your terminal for you to chat."


clean:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f  {} +
	rm -rf build/
	rm -rf dist/
	rm -rf *.egg-info
	rm -rf docs/_build

train-nlu:
	python3 -m rasa_nlu.train -c nlu_config.yml --data data/nlu_data.md -o models --fixed_model_name nlu --project current --verbose

train-core:
	python3 -m rasa_core.train -d domain.yml -s data/stories.md -o models/current/dialogue -c policies.yml

train-interactive:
	python3 -m rasa_core.train interactive -o models/dialogue   -d domain.yml -c policies.yml  -s data/stories.md  --nlu models/current/nlu  --endpoints endpoints.yml

cmdline:
	python3 -m rasa_core.run -d models/current/dialogue -u models/current/nlu --endpoints endpoints.yml
	
action-server:
	python3 -m rasa_core_sdk.endpoint --actions actions

nlu-evaluate:
	python3 -m rasa_nlu.evaluate     --data data/nlu_data.md     --model models/current/nlu

core-evaluate:
	python3 -m rasa_core.evaluate   --core models/dialogue    --stories data/stories.md   -o evaluateresults

nlu-test:
	python3 -m rasa_nlu.evaluate     --data test/nlu_data.md     --model models/current/nlu

core-test:
	python3 -m rasa_core.evaluate --core models/dialogue --stories    test_stories.md -o testresults

nlu-crossvalidate:
	python3 -m rasa_nlu.evaluate --data data/nlu_data.md   --config nlu_config.yml --mode crossvalidation

