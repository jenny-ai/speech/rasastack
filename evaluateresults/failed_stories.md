## Generated Story ask_to_wake_up_03_hapypath
* ask to wake up at a certain time{"time": "9 pm", "day": "today"}
    - slot{"day": "today"}
    - slot{"time": "9 pm"}
    - slot{"day": "today"}
    - slot{"time": "9 pm"}
    - wake_form
    - form{"name": "wake_form"}
    - slot{"time": "9 pm"}
    - slot{"day": "today"}
    - slot{"time": "9 pm"}
    - slot{"day": "today"}
    - form{"name": null}
    - slot{"requested_slot": null}
* affirm
    - action affirm wake
    - slot{"time": null}
    - slot{"day": null}
    - action_restart   <!-- predicted: action_listen -->
    - action_listen   <!-- predicted: utter_happy -->


